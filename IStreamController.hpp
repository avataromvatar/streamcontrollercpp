/*
 * IStreamController.hpp
 *
 *  Created on: 25 сент. 2020 г.
 *      Author: avatar
 */

#ifndef ISTREAMCONTROLLER_HPP_
#define ISTREAMCONTROLLER_HPP_
#include "iostream"
#include "functional"
#include "memory"
#include "vector"
namespace Stream {

//template<
//    typename char_type = char,
//    typename traits_type = std::char_traits< char_type > >
class IStreamController
{
public:

	enum eOptions
	{
		DataKeeperON =1,

	};
	//call when data read from another side
	typedef std::function<void (int err,int len)> cb_on_write;
	//call when data write to another side
	typedef std::function<void (int err,int len,std::istream &inStream)> cb_on_read;


	virtual int write(const char *writeFrom,int len)=0;
	virtual int read(char *saveHere,int maxLen)=0;
	virtual unsigned int getAvailableMemToWrite()=0;
	virtual unsigned int getAvailableDataToRead()=0;
	virtual unsigned int getSizeBuffer()=0;
//	virtual std::ostream& operator<<(std::ostream & out, const IStreamController & streamController) = 0;
//	virtual std::istream& operator>>(std::istream & in, IStreamController & streamController) = 0;
	virtual void setCallback(cb_on_write on_write, cb_on_read on_read) = 0;
//	virtual int addListener(cb_on_write on_write)=0;
//	virtual int addListener(cb_on_read on_read)=0;
	virtual void setOption(eOptions opt)=0;
	virtual bool isOtherSide() = 0;
//	virtual bool isRight() = 0;
	virtual bool isGood() = 0;
	virtual bool isBad() = 0;
	virtual bool isDataKeeper() = 0;
	virtual IStreamController* getSteamControllerFromOtherSide()=0;
//	virtual std::streambuf *geStreambuf() = 0;
//	virtual IAStream<char> &getStream() = 0;
protected:

	//GET SECTION
		///Get character on underflow and advance position
		virtual int uflow()= 0;
		///Get character on underflow
		virtual int underflow()= 0;
		///Get number of characters available
		virtual std::streamsize showmanyc()= 0;
		///Get sequence of characters
		virtual std::streamsize xsgetn (char* s, std::streamsize n)= 0;
		///Put character back in the case of backup underflow
		virtual int pbackfail (int c = EOF)= 0;

		///PUT SECTION
		///Put sequence of characters
		virtual std::streamsize xsputn (const char* s, std::streamsize n)= 0;
		///Put character on overflow
		virtual int overflow (int c = EOF)= 0;

	virtual void notifyReader(std::streamsize len)=0;
	virtual void notifyWriter(std::streamsize len)=0;

};

}//namespace Stream

#endif /* ISTREAMCONTROLLER_HPP_ */
