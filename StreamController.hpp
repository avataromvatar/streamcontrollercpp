/*
 * StreamController.hpp
 *
 *  Created on: 25 сент. 2020 г.
 *      Author: avatar
 *
 *  Здесь Stream это двунаправленный поток типа pipe у которой есть "левая" сторона и "правая"
 *  Он состоит из 2 буферов с фиксированной длинной. Каждую сторону Steam обслуживает SteamController
 *
 *  StreamController - это какая то сторона обслуживающая Steam, либо OtherSide либо нет :)
 *  Он наследует steambuf что делет его пригодным для работы с std::iostream
 *
 * Side(left)					OtherSide(right)
 *  leftOut	->    |	      buffer fix size				| ->right IN
 *  			l.begin                ^l.current       l.end
 *              r.begin ^r.current     ^r.end
 *   У другого буфера наоборот letfIN rightOUT
 *   у читающего конец(r.end = l.current) буфера перемещается в зависемости от кол-во байт записанных в буфер
 *
 *
 *
 *	Потокобезопасный (левая и правая сторона могут работать в разных потоках
 *	Обнуления буфферов происходит тогда когда читающий догоняет пишущего
 *
 *	TODO isKeeperData =false Он нехранит данные после оповещения readera о приходе данных сразу удалять
 *	и перемещать указатели на начало
 *	TODO isGood уточниться после чего я стану плохим и реализовать это (например в меня больше не входит)
 *	TODO проверить в боевых условиях с разными потоками
 *
 *
 */

#ifndef STREAMCONTROLLER_HPP_
#define STREAMCONTROLLER_HPP_

#include "mutex"
#include <map>
#include "iostream"
//#include "IAStream.hpp"
#include "IStreamController.hpp"


namespace Stream {



class StreamController :public std::streambuf,public IStreamController
{
public:

	StreamController(int size = 65535);
	virtual ~StreamController();

	virtual int write(const char *writeFrom,int len);

	virtual int read(char *saveHere,int maxLen);

	virtual char * getOutputBuffer();
	virtual int commit(int len);
	virtual std::shared_ptr<std::vector<char>> getInputBuffer();


	virtual unsigned int getAvailableMemToWrite();
	virtual unsigned int getAvailableDataToRead();
	virtual unsigned int getSizeBuffer();

//	friend std::ostream& operator<<(std::ostream & out, StreamController & streamController);
//	friend std::streambuf& operator>>(std::streambuf & in, StreamController & streamController);
	std::streambuf& operator<<(std::streambuf & out);
	std::streambuf& operator<<(const char * data);
	std::streambuf& operator>>(std::streambuf & in);
//	std::ostream& operator<<(std::ostream & out);
//	std::istream& operator>>(std::istream & in);
	virtual void setCallback(cb_on_write on_write, cb_on_read on_read);
//	int addListener(cb_on_write on_write);
//	int addListener(cb_on_read on_read);
	virtual void setOption(eOptions opt);
	virtual bool isOtherSide();
	virtual bool isGood();
	virtual bool isBad();
	virtual bool isDataKeeper();
	virtual IStreamController *getSteamControllerFromOtherSide();
	virtual void flush();
	std::streambuf * streambuf(){return this;}
//	std::streambuf *geStreambuf();

//	IAStream<char> &getStream();
protected:
//	std::vector<std::shared_ptr<IStreamController>> otherSide;
	//GET SECTION
		///Get character on underflow and advance position
	virtual int uflow();
		///Get character on underflow
	virtual int underflow();
		///Get number of characters available
		std::streamsize showmanyc();
		///Get sequence of characters
		std::streamsize xsgetn (char* s, std::streamsize n);
		///Put character back in the case of backup underflow
		int pbackfail (int c = EOF);

		///PUT SECTION
		///Put sequence of characters
		std::streamsize xsputn (const char* s, std::streamsize n);
		///Put character on overflow
		int overflow (int c = EOF);

		virtual int sync();




//	std::vector<cb_on_write> _listenersOnWrite;
//	std::vector<cb_on_write> _listenersOnRead;

	virtual void notifyReader(std::streamsize len);
	virtual void notifyWriter(std::streamsize len);

//	std::streambuf _buffer;
	cb_on_write _onWrite = NULL;
	cb_on_read _onRead = NULL;



	char *thisInOtherOut = NULL;
	char *otherInThisOut = NULL;

	StreamController *_other;
	std::mutex _mutexIN;
	std::mutex _mutexOUT;
	std::mutex _mutexSys;
private:
	//just for otherЫ
	StreamController(StreamController *mother,int size,char *thisInOtherOut, char*otherInThisOut);

	bool _isOtherSide=false;
	bool _isGood = true;
	bool _isDataKeeper = true;
	int _size = 0;

};

} /* namespace Stream */

#endif /* STREAMCONTROLLER_HPP_ */
