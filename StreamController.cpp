/*
 * StreamController.cpp
 *
 *  Created on: 25 сент. 2020 г.
 *      Author: avatar
 */
#include "string.h"
#include "StreamController.hpp"

namespace Stream {

StreamController::StreamController(int size) {
	_size = size;
	thisInOtherOut = new char[_size];
	otherInThisOut = new char[_size];
	setp(thisInOtherOut, thisInOtherOut + _size);
	setg(otherInThisOut, otherInThisOut, otherInThisOut);


	printf("%s  created size:%d G:%d %d %d  P:%d %d %d\n",
					_isOtherSide ? "Other StreamController" : "StreamController",
					_size, _M_in_beg, _M_in_cur, _M_in_end, _M_out_beg, _M_out_cur,
					_M_out_end);
	_other = new StreamController(this, size, thisInOtherOut, otherInThisOut);

}

StreamController::StreamController(StreamController *mother, int size,
		char *thisInOtherOut, char *otherInThisOut) {
	_isOtherSide = true;
	_size = size;
	_other = mother;
	this->thisInOtherOut = thisInOtherOut;
	this->otherInThisOut = otherInThisOut;
	setp(otherInThisOut, otherInThisOut + _size);
	setg(thisInOtherOut, thisInOtherOut, thisInOtherOut);
	printf("%s  created size:%d G:%d %d %d  P:%d %d %d\n",
						_isOtherSide ? "Other StreamController" : "StreamController",
						_size, _M_in_beg, _M_in_cur, _M_in_end, _M_out_beg, _M_out_cur,
						_M_out_end);
}

StreamController::~StreamController() {
	// TODO Auto-generated destructor stub
	if(!_isOtherSide)
	{
	delete _other;
	delete[] otherInThisOut;
	delete[] thisInOtherOut;

	}
}

IStreamController* StreamController::getSteamControllerFromOtherSide() {
	return _other;
}
//std::ostream& StreamController::operator <<(std::ostream &out,
//		const IStreamController &streamController) {
//	StreamController *other = static_cast<StreamController*>( streamController.otherSide);
//
//
//}
//
//std::istream& StreamController::operator >>(std::istream &in,
//		IStreamController &streamController) {
//}

void StreamController::setCallback(cb_on_write on_write, cb_on_read on_read) {
	std::lock_guard<std::mutex> lock(_mutexSys);

	_onRead = on_read;
	_onWrite = on_write;
}

void StreamController::setOption(eOptions opt) {
}

bool StreamController::isGood() {
	return _isGood;
}

bool StreamController::isBad() {
	if (_isGood)
		return false;
	else
		return true;
}

bool StreamController::isDataKeeper() {
	return _isDataKeeper;
}

int StreamController::write(const char *writeFrom, int len) {
	return xsputn(writeFrom,len);
}

int StreamController::read(char *saveHere, int maxLen) {
	return xsgetn(saveHere,maxLen);
}

unsigned int StreamController::getAvailableMemToWrite() {
	return showmanyc();
}

unsigned int StreamController::getAvailableDataToRead() {
	std::lock_guard<std::mutex> lock(_mutexIN);
	return egptr() - gptr();
}

unsigned int StreamController::getSizeBuffer() {
	return _size;
}

bool StreamController::isOtherSide() {
	return _isOtherSide;
}

void StreamController::notifyReader(std::streamsize len) {

	std::streamsize l = 0;
	{
		std::lock_guard<std::mutex> lock(_mutexIN);
		_M_in_end += len; // передвигаем конец входного блока

		printf("%s  notifyReader len:%d G:%d %d %d  P:%d %d %d\n",
				_isOtherSide ? "Other StreamController" : "StreamController",
				len, _M_in_beg, _M_in_cur, _M_in_end, _M_out_beg, _M_out_cur,
				_M_out_end);
		l = _M_in_end - _M_in_cur;

	}
	{
		std::lock_guard<std::mutex> lock(_mutexSys);


		if (_onRead) {

			std::istream is(this);
			_onRead(0, l, is);

		}
	}

}

void StreamController::notifyWriter(std::streamsize len) {
	printf("%s  notifyWriter len:%d G:%d %d %d  P:%d %d %d\n",
			_isOtherSide ? "Other StreamController" : "StreamController", len,
			_M_in_beg, _M_in_cur, _M_in_end, _M_out_beg, _M_out_cur,
			_M_out_end);
	std::lock_guard<std::mutex> lock(_mutexSys);
	if (_onWrite) {
		_onWrite(0, len);
	}
}

int StreamController::uflow() {
	printf("%s  uflow G:%d %d %d  P:%d %d %d\n",
			_isOtherSide ? "Other StreamController" : "StreamController",
			_M_in_beg, _M_in_cur, _M_in_end, _M_out_beg, _M_out_cur,
			_M_out_end);
	std::lock_guard<std::mutex> lock(_mutexIN);
	if (gptr() + 1 < egptr()) {
		gbump(1);
		return *(gptr() - 1);
	}
	return traits_type::eof();
}

int StreamController::underflow() {
	printf("%s  underflow G:%d %d %d  P:%d %d %d\n",
			_isOtherSide ? "Other StreamController" : "StreamController",
			_M_in_beg, _M_in_cur, _M_in_end, _M_out_beg, _M_out_cur,
			_M_out_end);
	std::lock_guard<std::mutex> lock(_mutexIN);
	if (gptr() < egptr()) {
		return *gptr();
	}
	return traits_type::eof();
}

std::streamsize StreamController::showmanyc() {
	std::lock_guard<std::mutex> lock(_mutexOUT);
	return epptr() - pptr();
}

std::streamsize StreamController::xsgetn(char *s, std::streamsize n) {

	std::lock_guard<std::mutex> lock(_mutexIN);
	char_type *p = gptr();
	std::streamsize av = 0;

	printf("%s  xsgetn n:%d G:%d %d %d  P:%d %d %d\n",
			_isOtherSide ? "Other StreamController" : "StreamController", n,
			_M_in_beg, _M_in_cur, _M_in_end, _M_out_beg, _M_out_cur,
			_M_out_end);

	p = gptr();
	av = egptr() - gptr(); //Available
	if (av == 0)
		return traits_type::eof();
	if (n < av)
		av = n;
	for (int i = 0; i < av; i++) {
		s[i] = p[i];
	}
	gbump(av);

	_other->notifyWriter(av);
	if (gptr() == _other->pptr()) {
		std::lock_guard<std::mutex> lock(_mutexOUT);
		//сброс позиций к нулевым тогла когда все данные будут считаны
		_other->setp(_other->pbase(), _other->epptr()); //TODO проверить что она устанавливет _M_out_cur  в pbase()
		setg(eback(), eback(), eback());

		printf("%s  xsgetn SynctoBegin G:%d %d %d  P:%d %d %d\n",
				_isOtherSide ? "Other StreamController" : "StreamController",
				_M_in_beg, _M_in_cur, _M_in_end, _M_out_beg, _M_out_cur,
				_M_out_end);
	}

	return av;

}

int StreamController::pbackfail(int c) {
	printf("%s created pbackfail c:%d G:%d %d %d  P:%d %d %d\n",
			_isOtherSide ? "Other StreamController" : "StreamController", c,
			_M_in_beg, _M_in_cur, _M_in_end, _M_out_beg, _M_out_cur,
			_M_out_end);
	return traits_type::eof();
}

std::streamsize StreamController::xsputn(const char *s, std::streamsize n) {
	std::streamsize av = 0;

	{
		std::lock_guard<std::mutex> lock(_mutexOUT);
	printf("%s  xsputn n:%d  G:%d %d %d  P:%d %d %d\n",
			_isOtherSide ? "Other StreamController" : "StreamController", n,
			_M_in_beg, _M_in_cur, _M_in_end, _M_out_beg, _M_out_cur,
			_M_out_end);
	char_type *p = pptr();
	av = epptr() - pptr(); //Available
	if (av == 0)
		return traits_type::eof();
	if (n < av)
		av = n;
	for (int i = 0; i < av; i++) {
		p[i] = s[i];
	}
	pbump(av);
	} //_mutexOUT relised

	{

	_other->notifyReader(av);
	}
	return av;
}

int StreamController::overflow(int c) {
	printf("%s  overflow c:%d  G:%d %d %d  P:%d %d %d\n",
			_isOtherSide ? "Other StreamController" : "StreamController", c,
			_M_in_beg, _M_in_cur, _M_in_end, _M_out_beg, _M_out_cur,
			_M_out_end);
	return traits_type::eof();
}

void StreamController::flush() {
	std::lock_guard<std::mutex> lock(_mutexOUT);
	std::lock_guard<std::mutex> lock1(_other->_mutexOUT);

	int ret = sync();
	ret = _other->sync();

}

int StreamController::sync() {

	std::lock_guard<std::mutex> lock(_mutexIN);

	setp(pbase(), epptr());
	setg(eback(), eback(), eback());
return 0;
}

std::streambuf& StreamController::operator <<(std::streambuf &out) {

	std::lock_guard<std::mutex> lock(_mutexOUT);
	printf("<< size in:%d this:%d",out.in_avail(),getSizeBuffer());
		int c=0;
		for(int i=0;i<getAvailableMemToWrite()&& c!=traits_type::eof();i++)
		{
			c = out.sbumpc();
			if(c!=traits_type::eof())
			{
				sputc(c);
			}
		}
		return *this;

}

std::streambuf& StreamController::operator >>(std::streambuf &in) {

	printf(">> size in:%d this:%d",in.in_avail(),getSizeBuffer());
	int ret = in.sputn(_M_in_cur, getAvailableDataToRead());
		if(ret != traits_type::eof())
			gbump(ret);
		return *this;


}

//std::ostream& StreamController::operator <<(std::ostream &out) {
//	std::ostream os(streambuf());
//	return os<<&out;
//}
//
//std::istream& StreamController::operator >>(std::istream &in) {
//	std::istream is(streambuf());
//		return in>>&is;
//}

std::streambuf& StreamController::operator <<(const char *data) {
	int l = strlen(data);
	write(data,l);
	return *streambuf();
}



std::shared_ptr<std::vector<char> > StreamController::getInputBuffer() {
	std::vector<char> *ret = new std::vector<char>();
	int count = getAvailableDataToRead();
	for(int i=0;i<count;i++)
	{
		ret->push_back(uflow());
	}
	return std::shared_ptr<std::vector<char>>(ret);
}

char* StreamController::getOutputBuffer() {
	return pptr();
}

int StreamController::commit(int len) {
	pbump(len);
	return 0;
}

} /* namespace Stream */
