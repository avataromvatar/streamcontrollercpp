/*
 * main.cpp
 *
 *  Created on: 28 сент. 2020 г.
 *      Author: avatar
 */
#include "iostream"
#include "StreamController.hpp"

using namespace Stream;

int main()
{
	StreamController sm(5);
	StreamController *smO = static_cast<StreamController*>(sm.getSteamControllerFromOtherSide());
	StreamController sm1(6);
	StreamController *sm1O = static_cast<StreamController*>(sm1.getSteamControllerFromOtherSide());

	std::istream is(&sm);
	std::istream isO(static_cast<StreamController*>(sm.getSteamControllerFromOtherSide()));
	std::istream is1(&sm1);
	std::istream isO1(static_cast<StreamController*>(sm1.getSteamControllerFromOtherSide()));
	std::ostream os(&sm);
	std::ostream osO(static_cast<StreamController*>(sm.getSteamControllerFromOtherSide()));
	std::ostream os1(&sm1);
	std::ostream osO1(static_cast<StreamController*>(sm1.getSteamControllerFromOtherSide()));

	sm<<"Hi";
	std::cout<<smO->streambuf()<<std::endl<<"sdfsdfsdf\n"<<std::endl;

}

